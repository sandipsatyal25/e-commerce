import React from 'react';
import { StyleSheet, 
        Text, 
        View,
        TouchableOpacity,
        TextInput,
        AppRegistry,
        Alert,
     } from 'react-native';
import firebase from 'firebase';
export default class Logger extends React.Component {
    constructor(props){
        // var myText;
        var utc = new Date().toJSON().slice(0,10).replace(/-/g,':');
        var time = new Date().toTimeString().split(' ')[0].split(':');
        var RN= time[0]+':'+time[1]+':'+time[2];
        super(props);
        this.state = {
        action:'',
        data:{},
    }
}
 componentWillMount(){
     var config = {
    apiKey: "AIzaSyB-4OsC6Z0ad4V81ZujC52iArzJRGgLJpw",
    authDomain: "log-me-6713a.firebaseapp.com",
    databaseURL: "https://log-me-6713a.firebaseio.com",
    projectId: "log-me-6713a",
    storageBucket: "log-me-6713a.appspot.com",
    messagingSenderId: "1088568571054"
  };
  firebase.initializeApp(config);
 
  logged=()=>{
    var utc = new Date().toJSON().slice(0,10).replace(/-/g,':');
    var time = new Date().toTimeString().split(' ')[0].split(':');
    var RN= time[0]+':'+time[1]+':'+time[2];
    // console.log(RN+':'+time[2]);
    if(this.state.action==""){
        Alert.alert('Nothing to log');
    }
    else{
    firebase.database().ref(utc + '/' + RN).set(
        {
          action:this.state.action
        }
      ).then(()=>{
        console.log('information added')
      }).catch(err=>{
        console.log(err)
      })
      this.setState(()=>{
        return{action:''}
    }
)
    console.log(this.state.action)} 
}
firebase.database().ref(this.utc).once('value', (data)=>{
    this.setState((data)=>{
        return{data:this.data}
    })
    console.log(this.state.data);
});
}
  render() {
    return (
      <View>
        <TextInput onChangeText={(text)=>{this.setState({action:text})}} value={this.state.action} style={css.input}/>
        <TouchableOpacity onPress={logged}>
        <Text style={css.logme}>Log this</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const css = StyleSheet.create({
    logme:{
        fontSize:30, 
        textAlign:'center', 
        color:'white', 
        backgroundColor:'black', 
        width:100,
        alignSelf:'center'
    },
    input:{
        borderBottomWidth:2,
        marginBottom:10,
        margin:5
    }
})
AppRegistry.registerComponent('Logger', ()=>Logger);