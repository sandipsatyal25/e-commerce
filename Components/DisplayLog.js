import React from 'react';
import { StyleSheet, 
        Text, 
        View,
        AppRegistry,
     } from 'react-native';
export default class DisplayLog extends React.Component {
    
    render(){
    return(
        <View style={css.mylogs}>
        <Text>Today's log</Text>
        </View>
    )
}
}
const css = StyleSheet.create({
    mylogs:{
        fontSize:22
    }
})
AppRegistry.registerComponent('DisplayLog', ()=>DisplayLog);