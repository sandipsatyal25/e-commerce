import React from 'react';
import { 
        Text,
        SafeAreaView,
        StyleSheet 
} from 'react-native';
import TopBar from './Components/TopBar';
import Logger from './Components/Logger';
import DisplayLog from './Components/DisplayLog';
export default class App extends React.Component {
  render() {
    return (
      <SafeAreaView>
        <TopBar />
        <Text style={css.heading}>LOGGER</Text>
        <DisplayLog />
        <Logger />
      </SafeAreaView>
    );
  }
}

const css = StyleSheet.create({
  heading:{
    backgroundColor:'black', 
    color:'white', 
    padding:10, 
    marginBottom:15,
    textAlign:'center', 
    fontSize:18
  }
})